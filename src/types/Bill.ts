import type BillDetail from "./BillDetail";
import type Employee from "./Employee";
export default interface Bill {
  id?: number;
  shop_name?: String;
  date?: string;
  time?: string;
  total?: number;
  buy?: number;
  change?: number;
  employee?: Employee;
  employeeId?: number;
  billDetails?: BillDetail[]
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
