import type Order from "./Order";

export default interface Store {
  id?: number;
  name: string;
  address: string;
  tel: string;
  orders?: Order;
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}