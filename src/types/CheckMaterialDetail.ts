import type CheckMaterial from "./CheckMaterial";
import type Material from "./material";
export default interface CheckMaterialDetail {
  id?: number;
  name?: string;
  qty_last?: number;
  qty_remain?: number;
  qty_expire?: number;
  check_mateial?: CheckMaterial;
  materialId?: number;
  material?: Material;
  createDate?: Date;
  updateDate?: Date;
  deletedDate?: Date;
}

