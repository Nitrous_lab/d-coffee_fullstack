export default interface Customer {
  id?: number;
  name: string;
  tel: string;
  point: number;
  startDate?: string;
  createdDate?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}