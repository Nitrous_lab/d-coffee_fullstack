export default interface Salary {
    id?: number;
    date: string;
    workhour: Number;
    salary: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
  }