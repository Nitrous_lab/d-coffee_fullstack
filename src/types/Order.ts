import type Customer from "./Customer";
import type Employee from "./Employee";
import type OrderItem from "./OrderItem";
import type Store from "./Store";

export default interface Order {
    id?: number
    amount?: number;
    total?: number;
    recevied?: number
    change?: number
    payment?: string;
    discount?: string;
    empId?: number;
    cusId?: number;
    tel?: string;
    storeId?: number;
    store?: Store;
    customer?: Customer;
    employee?: Employee;
    orderItems: OrderItem[];
}