import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:3000",
});
function delay(time: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
}

instance.interceptors.request.use(async function (config) {
  const token = localStorage.getItem("token");
  if(token){
    config.headers.Authorization = `Bearer ${token}`;
  }
  await delay(1000);
  return config;
  
});



export default instance;