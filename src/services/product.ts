import type Product from "@/types/Product";
import http from "./axios";

function getProduct() {
  return http.get("/products");
}
function saveProduct(product: Product & {files:File[]}) {
  const formDate = new FormData();
  formDate.append("name",product.name);
  formDate.append("price",`${product.price}`);
  formDate.append("type",product.type);
  formDate.append("size",product.size);
  formDate.append("file",product.files[0])
  formDate.append("categoryId",`${product.categoryId}`);
  return http.post("/products", formDate,{headers: {
    "Content-Type" : "multipart/form-data"
  }});
}
function updateProduct(id: number, product: Product) {
  return http.patch(`/products/${id}`, product);
}
function updateProductImg(id: number, files:File[]) {
  const formDate = new FormData();
  formDate.append("file",files[0])
  return http.patch(`/products/${id}/image`, formDate,{headers: {
    "Content-Type" : "multipart/form-data"
  }});
}

function GetProductsByCategory(id:number){
  return http.get(`/products/category/${id}`)
}

function deleteProduct(id: number) {
  return http.delete(`/products/${id}`);
}
export default { getProduct, saveProduct, updateProduct, deleteProduct,updateProductImg ,GetProductsByCategory};