import type Bill from "@/types/Bill";
import http from "./axios";

function getBill() {
  return http.get("/bill");
}
function getBillById(id: number) {
  return http.get(`/bill/${id}`);
}
function saveBill(Bill: Bill) {
  return http.post("/bill", Bill);
}
function updateBill(id: number, Bill: Bill) {
  return http.patch(`/bill/${id}`, Bill);
}
function deleteBill(id: number) {
  return http.delete(`/bill/${id}`);
}
export default { getBill, getBillById, saveBill, updateBill, deleteBill };
