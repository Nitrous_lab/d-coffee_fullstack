import type BillDetail from "@/types/BillDetail";
import http from "./axios";

function getBillDetail() {
  return http.get("/bill");
}
function saveBillDetail(Bill: BillDetail) {
  return http.post("/bill", Bill);
}
function updateBillDetail(id: number, Bill: BillDetail) {
  return http.patch(`/bill/${id}`, Bill);
}
function deleteBillDetail(id: number) {
  return http.delete(`/bill/${id}`);
}
export default { getBillDetail, saveBillDetail, updateBillDetail, deleteBillDetail };