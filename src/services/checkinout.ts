import type Checkinout from "@/types/Checkinout";

import http from "./axios";

function getCheckinout() {
  return http.get("/Checkinout");
}
function saveCheckinout(Checkinout: Checkinout) {
  return http.post("/checkinout", Checkinout);
}
function updateCheckinout(Checkinout: Checkinout) {
  return http.patch(`/checkinout/`, Checkinout);
}
function deleteCheckinout(id: number) {
  return http.delete(`/checkinout/${id}`);
}
function getCheckinoutByEmp(email:string){
  return http.get(`/checkinout/email/${email}`);
}

function getCheckinoutStage(email: string){
  return http.get(`/checkinout/Stage/${email}`);
}
export default { getCheckinout, saveCheckinout, updateCheckinout, deleteCheckinout,getCheckinoutByEmp,getCheckinoutStage };