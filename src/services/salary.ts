import type Salary from "@/types/Salary";
import http from "./axios";

function getSalary() {
  return http.get("/Salarys");
}
function saveSalary(SummarySalary: Salary) {
  return http.post("/Salarys", SummarySalary);
}
function updateSalary(id: number, SummarySalary: Salary) {
  return http.patch(`/Salarys/${id}`, SummarySalary);
}
function deleteSalary(id: number) {
  return http.delete(`/Salarys/${id}`);
}
function getSalaryName() {
  return http.get("/Salarys/all");
}
export default { getSalary, saveSalary, updateSalary, deleteSalary,getSalaryName};