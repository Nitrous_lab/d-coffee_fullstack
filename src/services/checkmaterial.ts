import type CheckMaterial from "@/types/CheckMaterial";
import http from "./axios";

function getCheckMaterial() {
  return http.get("/check-material");
}
function saveCheckMaterial(CheckMaterial: CheckMaterial) {
  return http.post("/check-material", CheckMaterial);
}
function updateCheckMaterial(id: number, CheckMaterial: CheckMaterial) {
  return http.patch(`/check-material/${id}`, CheckMaterial);
}
function deleteCheckMaterial(id: number) {
  return http.delete(`/check-material/${id}`);
}

export default { getCheckMaterial, saveCheckMaterial, updateCheckMaterial, deleteCheckMaterial, };