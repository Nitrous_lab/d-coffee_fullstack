import type Employee from "@/types/Employee";
import http from "./axios";

function getEmployee() {
  return http.get("/Employees");
}
function saveEmployee(Employee: Employee) {
  return http.post("/Employees", Employee);
}
function updateEmployee(id: number, Employee: Employee) {
  return http.patch(`/Employees/${id}`, Employee);
}
function deleteEmployee(id: number) {
  return http.delete(`/Employees/${id}`);
}
function getEmployeeName() {
  return http.get("/Employees/all");
}
export default { getEmployee, saveEmployee, updateEmployee, deleteEmployee,getEmployeeName};