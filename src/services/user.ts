import type User from "@/types/User";
import http from "./axios";

function getUser() {
  return http.get("/Users");
}
function saveUser(User: User & {files:File[]}) {
  const formDate = new FormData();
  formDate.append("name",User.name);
  formDate.append("email",`${User.email}`);
  formDate.append("tel",`${User.tel}`);
  formDate.append("role",`${User.role}`);
  formDate.append("password",`${User.password}`);
  formDate.append("file",User.files[0])
  return http.post("/users", formDate,{headers: {
    "Content-Type" : "multipart/form-data"
  }});
  
}
function updateUserImg(id: number, files:File[]) {
  const formDate = new FormData();
  formDate.append("file",files[0])
  return http.patch(`/Users/${id}/image`, formDate,{headers: {
    "Content-Type" : "multipart/form-data"
  }});
}
function updateUser(id: number, User: User) {
  return http.patch(`/Users/${id}`, User);
}
function deleteUser(id: number) {
  return http.delete(`/Users/${id}`);
}
function getUserName() {
  return http.get("/users/all");
}

function getUserIsNotEmp() {
  return http.get("/users/IsNotEmp");
}
export default { getUser, saveUser, updateUser, deleteUser ,getUserName,updateUserImg,getUserIsNotEmp};