import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
function getUser() {
  const user = localStorage.getItem("user");
  const json = JSON.parse(user!)
  return json
}
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: HomeView,
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: false,
      }
    },
    {
      path: '/about',
      name: 'about',

      components: {
        default: () => import('../views/AboutView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: '/pos',
      name: 'pos',
      components: {
        default: () => import('../views/PointOfSellView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/product',
      name: 'product',
      components: {
        default: () => import('../views/ProductView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/user',
      name: 'user',
      components: {
        default: () => import('../views/UserView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
        requiresRole: ['manager'],
      }
    },
    {
      path: '/edUser',
      name: 'edUser',
      components: {
        default: () => import('../views/EditUserView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },

    {
      path: '/login',
      name: 'login',
      components: {
        default: () => import('../views/LoginView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'FullLayout',
      }
    },
    {
      path: '/customer',
      name: 'customer',
      components: {
        default: () => import('../views/CustomerView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/order',
      name: 'order',
      components: {
        default: () => import('../views/OrderView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/employee',
      name: 'employee',
      components: {
        default: () => import('../views/EmployeeView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
        requiresRole: ['manager'],
      }
    },
    {
      path: '/material',
      name: 'material',
      components: {
        default: () => import('../views/MaterialView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/schedule',
      name: 'schedule',
      components: {
        default: () => import('../views/EmployeeSchedule.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/check',
      name: 'check',
      components: {
        default: () => import('../views/CheckMaterialView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/billhistory',
      name: 'billhistory',
      components: {
        default: () => import('../views/BillHistoryView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
      }
    },
    {
      path: '/reset',
      name: 'reset',
      components: {
        default: () => import('../views/resetView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'FullLayout',
      }
    },
    {
      path: '/store',
      name: 'store',
      components: {
        default: () => import('../views/StoreView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
        requiresRole: ['manager'],
      }
    },
    {
      path: '/Chart',
      name: 'chart',
      components: {
        default: () => import('../views/Dashbord/DashBordView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true,
        requiresRole: ['manager'],
      }
    },
  ]
})
function isLogin() {
  const user = getUser()

  if (user) {
    return true;
  }
  return false;
}
router.beforeEach((to, from) => {
  // instead of having to check every route record with
  // to.matched.some(record => record.meta.requiresAuth)
  if (to.meta.requiresAuth && !isLogin()) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return {
      path: '/login',
      // save the location we were at to come back later
      query: { redirect: to.fullPath },
    }
  }

})

// router.beforeEach((to, from, next) => {
//   const requiresRole = to.meta.requiresRole
//   const user = getUser()
//   if (requiresRole && user.role !== requiresRole) {
//     next('/about');
//   } else {
//     next();
//   }
// })

export default router
