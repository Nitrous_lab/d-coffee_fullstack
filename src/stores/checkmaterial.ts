import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia';
import type CheckMaterial from '@/types/CheckMaterial';
import checkmaterialService from '@/services/checkmaterial';
import type CheckMaterialDetail from '@/types/CheckMaterialDetail';
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';

export const useCheckmaterialStore = defineStore('checkMaterial', () => {
  const dialog = ref(false);
  const checkMaterial = ref<CheckMaterial[]>([]);

  const editedCheckMat = ref<{ materialId: number, name: string, qty_last: number, qty_remain: number, qty_expire: number }>({ materialId: 0, name: "", qty_last: 0, qty_remain: 0, qty_expire: 0 })
  const items = ref<{ materialId: number, name: string, qty_last: number, qty_remain: number, qty_expire: number }[]>([]);
  const select = ref(false)
  const temp = ref()

  const Struser = localStorage.getItem("user");
  const user = JSON.parse(Struser!)


  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();

  watch(temp, (newTemp) => {
    if (newTemp) {
      editedCheckMat.value = { materialId: temp.value.id, name: temp.value.name, qty_last: temp.value.quantity, qty_expire: 0, qty_remain: 0 }
    }
  })

  watch(select, (newSelect) => {
    if (!newSelect) {
      editedCheckMat.value = { materialId: 0, name: "", qty_last: 0, qty_remain: 0, qty_expire: 0 }
      temp.value = null
    }
  })

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      items.value = []
    }
  })

  async function getCheckM() {
    loadingStore.isLoading = true;
    try {
      const res = await checkmaterialService.getCheckMaterial();
      checkMaterial.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Checkmeterial ได้")
    }
    loadingStore.isLoading = false;
  }

  function addCheckMD() {
    const index = items.value.findIndex((item) => item.materialId == editedCheckMat.value.materialId)
    if (index == -1) {
      items.value.push(editedCheckMat.value)
    } else {
      items.value[index].qty_expire = editedCheckMat.value.qty_expire
      items.value[index].qty_last = editedCheckMat.value.qty_last
      items.value[index].qty_remain = editedCheckMat.value.qty_remain
    }
    select.value = false
  }

  function delCheckMD(id: number) {
    const index = items.value.findIndex((item) => item.materialId == id)
    items.value.splice(index, 1)

  }

  function editCheckMD(item: { materialId: number, name: string, qty_last: number, qty_remain: number, qty_expire: number }) {
    select.value = true
    editedCheckMat.value = { materialId: item.materialId, name: item.name, qty_last: item.qty_last, qty_remain: item.qty_remain, qty_expire: item.qty_expire }
  }


  async function saveCheckMat() {
    loadingStore.isLoading = true;
    try {
      const res = await checkmaterialService.saveCheckMaterial({ employeeId: user.employee.id, check_material_detail: items.value })
      dialog.value = false;
      await getCheckM()
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Checkmeterial ได้")
    }
    loadingStore.isLoading = false;;
  }
  const search = ref("");

  const showCheckMaterial = computed(() => {
    return checkMaterial.value.filter((item) => {
      return item.id?.toString().match(search.value);
    })
  })

  async function deleteCheckMat(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await checkmaterialService.deleteCheckMaterial(id);
      await getCheckM();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Checkmeterial ได้")
    }
    loadingStore.isLoading = false;
  }


  return { checkMaterial, getCheckM, saveCheckMat, deleteCheckMat, dialog, editedCheckMat, addCheckMD, delCheckMD, editCheckMD, items, select, temp, search, showCheckMaterial }
})    