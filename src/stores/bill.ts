import { defineStore } from 'pinia'
import { computed, ref, watch } from "vue";
import BillHistoryService from "@/services/bill";
import type BillHistory from '@/types/Bill';
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Employee from '@/types/Employee';


export const useBillHistoryStore = defineStore("BillHistory", () => {
  const StrUser = localStorage.getItem("user");
  const user = JSON.parse(StrUser!)
  const BillHistory = ref<BillHistory[]>([]);
  const dialog = ref(false);
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const select = ref(false);
  const item = ref<{ materialId: number, name: string, price: number, amount: number }[]>([])
  const editedBillHistory = ref<BillHistory>({ shop_name: "", total: 0, buy: 0, change: 0, employeeId: user.employee.id, billDetails: item.value });
  const temp = ref()
  const add = ref<{ materialId: number, name: string, price: number, amount: number }>({ materialId: 0, name: "", price: 0, amount: 0 })
  const showDialog = ref(false);
  const billItem = ref<{ shop_name: string, total: number, buy: number, change: number, date: string, employee: Employee }>()
  const billDetailsItem = ref<{ id: number, name: string, amount: number, price: number, total: number }[]>([])



  watch(temp, (newtemp) => {
    if (newtemp) {
      add.value = { materialId: temp.value.id!, name: temp.value.name!, price: 0, amount: 0 }

    }
  })

  watch(([dialog, select]), ([newDialog, newSelect]) => {
    if (!newDialog) {
      item.value = [];
      editedBillHistory.value = { shop_name: "", total: 0, buy: 0, change: 0, employeeId: user.employee.id, billDetails: item.value }
    }
    if (!newSelect) {
      add.value = { materialId: 0, name: "", price: 0, amount: 0 }
      temp.value = null
    }
  })

  function addBillDeteil() {
    item.value.push(add.value)
    select.value = false

  }



  async function GetBillHistory() {
    loadingStore.isLoading = true;
    try {
      const res = await BillHistoryService.getBill();
      BillHistory.value = res.data;

    } catch (e) {
      console.log(e)
      messageStore.showError("ไม่สามารถดึงข้อมูล Bill ได้")
    }
    loadingStore.isLoading = false;


  }

  async function saveBillHistory() {
    loadingStore.isLoading = true;

    try {
      const res = await BillHistoryService.saveBill(editedBillHistory.value)
    } catch (e) {
      console.log(e)
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Bill ได้")
    }
    await GetBillHistory();
    dialog.value = false
    loadingStore.isLoading = false;
  }

  async function deleteBillHistory(id: number) {
    loadingStore.isLoading = true;
    try {

      const res = await BillHistoryService.deleteBill(id);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Bill ได้")
    }
    loadingStore.isLoading = false;
  }
  const search = ref("");

  const showBill = computed(() => {
    return BillHistory.value.filter((item) => {
      return (item.shop_name || '').toLowerCase().match((search.value || '').toLowerCase());
    })
  })

  async function getBillById(id: number) {
    showDialog.value = true;
    loadingStore.isLoading = true;
    try {
      const res = await BillHistoryService.getBillById(id);
      billItem.value = res.data;
      billDetailsItem.value = res.data.billDetails;
    } catch (e) {
      console.log(e)
    }
    loadingStore.isLoading = false;
  }


  return { BillHistory, GetBillHistory, dialog, saveBillHistory, deleteBillHistory, item, addBillDeteil, select, add, temp, editedBillHistory, showBill, search, showDialog, getBillById, billItem, billDetailsItem }
});
