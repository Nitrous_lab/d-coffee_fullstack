import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type User from '@/types/User'
import UserService from '@/services/user'
import router from '@/router'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import { useAuthStore } from './auth'

export const useUserStore = defineStore('User', () => {
  const User = ref<User[]>([]);
  const dialog = ref(false);
  const editedUser = ref<User & { files: File[] }>({ name: "", email: "", tel: "", role: "", image: "no_image_available.jpg", files: [], });
  const UserIsNotEmp = ref<User[]>([])
  const auth = useAuthStore()
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  watch(dialog, (newDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedUser.value = { name: "", email: "", tel: "", role: "", image: "no_image_available.jpg", files: [] };
    }
  });
  async function GetUserNotEmp() {
    loadingStore.isLoading = true;
    try {
      const res = await UserService.getUserIsNotEmp();
      UserIsNotEmp.value = res.data
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้")
    }
    loadingStore.isLoading = false;
  }


  async function GetUser() {
    loadingStore.isLoading = true;
    try {
      const res = await UserService.getUser();
      User.value = res.data
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้")
    }
    loadingStore.isLoading = false;
  }
  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await UserService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
        if(res.data.id === auth.account?.id){
          console.log(res.data)
           const user = JSON.stringify(res.data);
          localStorage.setItem("user", user);
          await auth.loadUser()
          console.log(auth.account)
       }
      } else {
        const res = await UserService.saveUser(editedUser.value);
      }
    

      dialog.value = false;
      await GetUser();
    } catch (e) {

      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล User ได้")
    }
    loadingStore.isLoading = false;

  }

  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await UserService.deleteUser(id);
      await GetUser();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล User ได้")
    }
    loadingStore.isLoading = false;
  }
  function editUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }

  function editCurrentUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }

  const search = ref("");

  const showUser = computed(() => {
    return User.value.filter((item) => {
      return (item.tel || '').toLowerCase().match((search.value || '').toLowerCase());
    })
  })

  return { GetUser, User, editUser, deleteUser, saveUser, dialog, editedUser, GetUserNotEmp, UserIsNotEmp, showUser, search, editCurrentUser }
})