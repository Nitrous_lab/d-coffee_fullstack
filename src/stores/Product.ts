import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type Product from '@/types/Product'
import ProductService from "@/services/product";
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';

export const useProductStore = defineStore('Product', () => {
  const dialog = ref(false);
  const Products = ref<Product[]>([]);

  const ProductsByCate = ref<Product[]>([]);
  const editedProduct  = ref<Product & { files: File[] }>({name:"", price:0, type: "", size: "",  image: "no_image_available.jpg",categoryId:0,files:[]});

  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  watch([dialog], ([newDialog], oldDialog)=>{
    if(!newDialog ) {
      editedProduct.value = {name:"", price:0, type: "", size: "",  image: "no_image_available.jpg",categoryId:0,files:[]};
    }
 
  });
  async function GetProductsByCategory(id:number){
    loadingStore.isLoading = true;
    try{
      const res = await ProductService.GetProductsByCategory(id)
      ProductsByCate.value = res.data
    } catch(e){
      console.log(e)

    }
    loadingStore.isLoading = false;
  }

  async function GetProducts(){
    loadingStore.isLoading = true;
    try{
      const res = await ProductService.getProduct();
      Products.value = res.data;
    }catch(e){
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้")
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct(){
    console.log(editedProduct.value)
    try {
      if(editedProduct.value.id) {
       
        const res = await ProductService.updateProduct(editedProduct.value.id, editedProduct.value);
        
      }else {
        const res = await ProductService.saveProduct(editedProduct.value);
      }

      dialog.value = false;
      await GetProducts();
    } catch(e) {
      console.log(e);
    }
  }

  async function deleteProduct(id:number) {
    try {
     
        const res = await ProductService.deleteProduct(id);

      await GetProducts();
    } catch(e) {
      console.log(e);
    }
  }
  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  const search = ref("");

  const showProduct = computed(() => {
    return Products.value.filter((item) => {
      return (item.name || '').toLowerCase().match((search.value || '').toLowerCase());
    })
  })

  return { GetProducts ,Products, dialog, editedProduct, saveProduct, editProduct, deleteProduct,GetProductsByCategory,ProductsByCate, search, showProduct }
})
