import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import ProductService from "@/services/product"
import CustomerService from '@/services/customer'
import USerService from '@/services/user'
import { useProductStore } from './Product'
import { useCustomerStore } from './Customer'
import { useUserStore } from './User'
import { useAuthStore } from './auth'

export const useUploadImgStore = defineStore('UploadImg',  () => {
    const ProductStore = useProductStore()
    const CustomerStore = useCustomerStore();
    const files  =ref<File[]>([]);
    const auth = useAuthStore()
    const url = ref();
    const Id = ref();
    const service = ref("")
    const UploadImg = ref(false);
    const UserStore = useUserStore();
    watch(UploadImg, newUploadImg => {
        if(!newUploadImg){
            files.value = [];
            Id.value = ref();
            service.value = "";
           }
    })

    function EditImg(id:number,services:string){
        service.value = services
        Id.value = id;
        UploadImg.value = true;
        
      }
    function onFileChange() {
        const file = files.value[0]
        url.value = URL.createObjectURL(file);
        return files.value;
    }

 async function Sendreq(){
    switch (service.value) {
        case "Product":
            try{
                const res = await ProductService.updateProductImg(Id.value,onFileChange());
              }catch(e){
                console.log(e)
              }
              UploadImg.value = false;
              await ProductStore.GetProducts();
              
            break;
        case "User":
            try{
                const res = await USerService.updateUserImg(Id.value,onFileChange());
                const user = JSON.stringify(res.data);
                localStorage.setItem("user", user);
                await auth.loadUser()
              }catch(e){
                console.log(e)
              }
              UploadImg.value = false;
              await UserStore.GetUser();
            break;
    }
    
 }

  return {EditImg,onFileChange,url,files,UploadImg,Sendreq }
})
