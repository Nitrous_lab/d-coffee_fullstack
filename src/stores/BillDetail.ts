import { defineStore } from "pinia";
import { ref, watch } from "vue";
import BillHistoryDetailService from "@/services/bill-detail";
import type BillHistory from '@/types/Bill';
import type BillDetail from "@/types/BillDetail";


export const useBillHistoryDetailStore = defineStore("BillHistoryDetail", () => {
  const BillHistoryDetail = ref<BillDetail[]>([]);
  const dialog = ref(false);
  const editedBillHistoryDetail = ref<BillDetail>({ name:"", amount: 0, price: 0, total: 0});
  
  watch(dialog, newDailog => {
    if(!newDailog){
      editedBillHistoryDetail.value = {name:"", amount: 0, price: 0, total: 0}
    }
  })

  async function GetBillHistoryDetail(){
   try{
    const res = await BillHistoryDetailService.getBillDetail();
    BillHistoryDetail.value = res.data;

   }catch(e){
    console.log(e)
   }

  }

  async function saveBillHistoryDetail(){
    try{
       const res = await BillHistoryDetailService.saveBillDetail(editedBillHistoryDetail.value)
       
    } catch(e){
        console.log(e)
    }
    await GetBillHistoryDetail();
    dialog.value = false
  }

  async function deleteBillHistoryDetail(id:number) {
    try {
     
        const res = await BillHistoryDetailService.deleteBillDetail(id);

      await GetBillHistoryDetail();
    } catch(e) {
      console.log(e);
    }
  }

  return { BillHistoryDetail ,GetBillHistoryDetail ,dialog,saveBillHistoryDetail,deleteBillHistoryDetail}
});
