import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Material from '@/types/material'
import MaterialService from '@/services/material'
import ReportService from '@/services/report'
import { watch } from 'vue'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
export const useMaterialStore = defineStore('Material', () => {
  const Material = ref<Material[]>([]);
  const editedMaterial = ref<Material>({ name: "", min_quantity: 0, quantity: 0, unit: '', price_per_unit: 0 })
  const dialog = ref(false)
  const MatId = ref<number>();
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const MaterialLow = ref<Material[]>([])

  watch(dialog, newDailog => {
    if (!newDailog) {
      editedMaterial.value = { name: "", min_quantity: 0, quantity: 0, unit: '', price_per_unit: 0 }
    }
  })

  async function GetLowMat(){
    try {
      const res = await ReportService.MaterialLow()
      MaterialLow.value = res.data;
    } catch (error) {
      console.log(error)
    }

  }

  async function getMaterial() {
    loadingStore.isLoading = true;
    try {
      const res = await ReportService.LoadMaterial();
      Material.value = res.data[0];

    } catch (e) {
      console.log(e)
      messageStore.showError("ไม่สามารถดึงข้อมูล Meterial ได้")
    }
    loadingStore.isLoading = false;
  }

  async function saveMaterial() {
    loadingStore.isLoading = true;
    try {
      const res = await MaterialService.saveMaterial(editedMaterial.value)

    } catch (e) {
      console.log(e)
    }
    await getMaterial();
    dialog.value = false
    loadingStore.isLoading = false;
  }
  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {

      const res = await MaterialService.deleteMaterial(id);

      await getMaterial();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Meterial ได้")
    }
    loadingStore.isLoading = false;
  }

  const search = ref("");

  const showMaterial = computed(() => {
    return Material.value.filter((item) => {
      return (item.name || '').toLowerCase().match((search.value || '').toLowerCase());
    })
  })

  function editMaterial(material: Material) {
    editedMaterial.value = JSON.parse(JSON.stringify(material));
    dialog.value = true;
  }


  return { Material, getMaterial, dialog, editedMaterial, saveMaterial,MaterialLow, deleteProduct, editMaterial,GetLowMat, showMaterial, search }
})
