import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import router from "@/router";
import { useMessageStore } from "./message";
import type User from "@/types/User";


export const useAuthStore = defineStore("auth", () => {
  
  const messageStore = useMessageStore();
  
  const Struser = ref();
  const account = ref<User>();

   async function loadUser(){
    const load =  localStorage.getItem('user');
    Struser.value = load!;
    account.value =  await JSON.parse(Struser.value);
  }

  const login = async (username: string, password: string): Promise<void> => {

    try {

      const res = await auth.login(username, password);

      const user = JSON.stringify(res.data.user);
      localStorage.setItem("user", user);
      localStorage.setItem("token", res.data.access_token);
      router.push("/");
    } catch (e) {
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }

  };
  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    localStorage.removeItem("store")
    router.replace("/login");
  };

  return { login, logout, loadUser,account};
});
