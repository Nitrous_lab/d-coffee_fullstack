import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Store from '@/types/Store';
import StoreService from '@/services/store'
import { watch } from 'vue';
import { useLoadingStore } from './loading';

export const useStoreStore = defineStore('store', () => {
  const dialog = ref(false);
  const store = ref<Store[]>([]);
  const editedStore = ref<Store>({ name: "", tel: "", address: "" });
  const currStore = ref<Store>({ name: "", tel: "", address: "" });
  const loadingStore = useLoadingStore();
  
  watch(dialog, (newDiaolg) => {
    if (!newDiaolg) {
      editedStore.value = { name: "", tel: "", address: "" };
    }
  })

  async function getStore() {
    loadingStore.isLoading = true;
    try {
      const res = await StoreService.getStore();
      store.value = res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveStore() {
    try {
      if (editedStore.value.id) {
        const res = await StoreService.updateStore(editedStore.value.id, editedStore.value);
      } else {
        const res = await StoreService.saveStore(editedStore.value);
      }
      await getStore();
      dialog.value = false
    } catch (e) {
      console.log(e);
    }
  }

  function editStore(Store: Store) {
    editedStore.value = JSON.parse(JSON.stringify(Store));
    dialog.value = true;
  }

  async function deleteStore(id: number) {
    try {
      const res = await StoreService.deleteStore(id);
      await getStore();
    } catch (e) {
      console.log(e);
    }
  }

  const search = ref("");

  const showStoreByTel = computed(() => {
    return store.value.filter((item) => {
      return (item.tel || '').toLowerCase().match((search.value || '').toLowerCase());
    })
  })

  async function getStoreById(id: number) {
    try {

      const res = await StoreService.getStoreById(id);

      const store = JSON.stringify(res.data);
      localStorage.setItem("store", store);
    } catch (e) {
      console.log(e);
    }

  };

  return { store, editedStore, getStore, getStoreById, saveStore, editStore, deleteStore, dialog, search, showStoreByTel, currStore }
})
