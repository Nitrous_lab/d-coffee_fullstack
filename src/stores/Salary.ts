import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import SummarySalaryService from '@/services/salary'
import type SummarySalary from '@/types/Salary'
import { useUserStore } from './User'

export const useSummarySalaryStore = defineStore('SummarySalary', () => {
  const UserStore = useUserStore();
    const SummarySalary = ref<SummarySalary[]>([]);
    const dialog = ref(false);
    const editedSummarySalary = ref<SummarySalary>({ date: "", workhour: 0, salary: 0 });
    const userId = ref<number>();

    watch(dialog, (newDialog) => {
        
        if (!newDialog) {
          editedSummarySalary.value = { date: "", workhour: 0, salary: 0  };
          userId.value =0;
        }
      });
    watch(userId, (newuserId) => {
      if(newuserId){
          const index = UserStore.UserIsNotEmp.findIndex((item) => item.id === userId.value);
         const email = UserStore.UserIsNotEmp[index].email;
         const name = UserStore.UserIsNotEmp[index].name;

         editedSummarySalary.value = { date: "", workhour: 0, salary: 0   }
         
      }
    })


      

    
    async function GetSummarySalary(){
        try {
            const res = await SummarySalaryService.getSalary();
            SummarySalary.value = res.data
        }catch(e){
            console.log(e);
        }
    }
    async function saveSummarySalary() {
        console.log(editedSummarySalary.value)
        try {
          if (editedSummarySalary.value.id) {

            const res = await SummarySalaryService.updateSalary(
              editedSummarySalary.value.id,
              editedSummarySalary.value
            );
          } else {
            const res = await SummarySalaryService.saveSalary(editedSummarySalary.value);
          }
    
          dialog.value = false;
          await GetSummarySalary();
        } catch (e) {
          
          console.log(e);
        }
    
      }
    
      async function deleteSummarySalary(id: number) {
       
        try {
          const res = await SummarySalaryService.deleteSalary(id);
          await GetSummarySalary();
          await UserStore.GetUserNotEmp();
        } catch (e) {
          console.log(e);
          
        }
       
      }
      function editSummarySalary(SummarySalary: SummarySalary) {
        editedSummarySalary.value = JSON.parse(JSON.stringify(SummarySalary));
        dialog.value = true;
      }
    


  return { GetSummarySalary,SummarySalary,editSummarySalary,deleteSummarySalary,saveSummarySalary,dialog,editedSummarySalary,userId}
})