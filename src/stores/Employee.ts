import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import EmployeeService from '@/services/employee'
import type Employee from '@/types/Employee'
import { useUserStore } from './User'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import employee from '@/services/employee'

export const useEmployeeStore = defineStore('Employee', () => {
  const UserStore = useUserStore();
    const Employee = ref<Employee[]>([]);
    const dialog = ref(false);
    const editedEmployee = ref<Employee>({ name: "", email: "", address: "",tel: "", position:"" });
    const userId = ref<number>();
    const loadingStore = useLoadingStore();
    const messageStore = useMessageStore();
    watch(dialog, (newDialog) => {
        
        if (!newDialog) {
          editedEmployee.value = { name: "", email: "", address: "",tel:"", position:""  };
          userId.value =0;
        }
      });
    watch(userId, (newuserId) => {
      if(newuserId){
          const index = UserStore.UserIsNotEmp.findIndex((item) => item.id === userId.value);
         const email = UserStore.UserIsNotEmp[index].email;
         const name = UserStore.UserIsNotEmp[index].name;

         editedEmployee.value = { name: name, email: email, address: "",tel:"", position:""  }
         
      }
    })

    async function GetEmployee(){
      loadingStore.isLoading = true;

        try {
            const res = await EmployeeService.getEmployee();
            Employee.value = res.data
        }catch(e){
            console.log(e);
            messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้")
        }
      loadingStore.isLoading = false;  
    }
    async function saveEmployee() {
      loadingStore.isLoading = true;
        console.log(editedEmployee.value)
        try {
          if (editedEmployee.value.id) {

            const res = await EmployeeService.updateEmployee(
              editedEmployee.value.id,
              editedEmployee.value
            );
          } else {
            const res = await EmployeeService.saveEmployee(editedEmployee.value);
          }
    
          dialog.value = false;
          await GetEmployee();
        } catch (e) {
          
          console.log(e);
          messageStore.showError("ไม่สามารถบันทึกข้อมูล Employee ได้")
        }
      loadingStore.isLoading = false;
    
      }
    
      async function deleteEmployee(id: number) {
        loadingStore.isLoading = true;
        try {
          const res = await EmployeeService.deleteEmployee(id);
          await GetEmployee();
          await UserStore.GetUserNotEmp();
        } catch (e) {
          console.log(e);
          messageStore.showError("ไม่สามารถลบข้อมูล Employee ได้")
        }
        loadingStore.isLoading = false;
      }
      function editEmployee(employee: Employee) {
        loadingStore.isLoading = true;
        editedEmployee.value = JSON.parse(JSON.stringify(employee));
        dialog.value = true;
        loadingStore.isLoading = false;
      }
    
      const search = ref("");

      const showEmployee = computed(() => {
        return Employee.value.filter((item) => {
          return (item.tel || '').toLowerCase().match((search.value || '').toLowerCase())
        })
      })
      const showIdEmployee = computed(() => {
        return Employee.value.filter((item) => {
          return item.id?.toString().match(search.value);
      })
      })

  return { GetEmployee,Employee,editEmployee,deleteEmployee,saveEmployee,dialog,editedEmployee,userId, search, showEmployee, showIdEmployee}
})