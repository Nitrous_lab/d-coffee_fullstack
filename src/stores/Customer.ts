import { ref, computed, } from 'vue'
import { defineStore } from 'pinia'
import type Customer from '@/types/Customer'
import CustomerService from "@/services/customer";
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';
import { watch } from 'vue';

export const useCustomerStore = defineStore('Customer', () => {
  const dialog = ref(false);
  const Customers = ref<Customer[]>([]);
  const editedCustomer = ref<Customer>({ name: "", tel: "", point: 0 });
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedCustomer.value = { name: "", tel: "", point: 0 }
    }
  })

  async function GetCustomers() {
    loadingStore.isLoading = true;
    try {
      const res = await CustomerService.getCustomer();
      Customers.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้")
    }
    loadingStore.isLoading = false;
  }
  async function saveCustomer() {
    loadingStore.isLoading = true;
    try {
      if (editedCustomer.value.id) {
        const res = await CustomerService.updateCustomer(editedCustomer.value.id, editedCustomer.value);
      } else {
        const res = await CustomerService.saveCustomer(editedCustomer.value);
      }

      dialog.value = false;
      await GetCustomers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Customer ได้")
    }
    loadingStore.isLoading = false;
  }
  async function deleteCustomer(id: number) {
    loadingStore.isLoading = true;
    try {

      const res = await CustomerService.deleteCustomer(id);

      await GetCustomers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Customer ได้")
    }
    loadingStore.isLoading = false;
  }
  function editCustomer(customer: Customer) {
    loadingStore.isLoading = true;

    editedCustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
    loadingStore.isLoading = false;

  }

  const search = ref("");

  const showCustomer = computed(() => {
    return Customers.value.filter((item) => {
      return (item.tel || '').toLowerCase().match((search.value || '').toLowerCase());
    })
  })

  return { GetCustomers, editedCustomer, Customers, dialog, editCustomer, saveCustomer, deleteCustomer, search, showCustomer }
})