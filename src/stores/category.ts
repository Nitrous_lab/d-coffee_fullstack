import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import CategoryService from '@/services/category'
import type Category from '@/types/Category'
import { useLoadingStore } from './loading'

export const useCategoryStore = defineStore('Category', () => {
  const categories = ref<Category[]>([])
  async function Getcategories(){

    try{
      const res = await CategoryService.getCategory();
      categories.value = res.data;
    }catch(e){
      console.log(e);

    }
  }
  return { Getcategories,categories }
})
